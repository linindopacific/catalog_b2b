<?php

namespace Config;

class Auth extends \Myth\Auth\Config\Auth
{
	/**
	 * --------------------------------------------------------------------
	 * Default User Group
	 * --------------------------------------------------------------------
	 *
	 * The name of a group a user will be added to when they register,
	 * i.e. $defaultUserGroup = 'guests'.
	 *
	 * @var string
	 */
	public $defaultUserGroup = 'guests';

	/**
	 * --------------------------------------------------------------------
	 * Libraries
	 * --------------------------------------------------------------------
	 *
	 * @var array
	 */
	public $authenticationLibs = [
		'local' => 'Myth\Auth\Authentication\LocalAuthenticator',
		'jwt' => 'LPI\Auth\Authentication\JwtAuthenticator',
	];

	/**
	 * --------------------------------------------------------------------
	 * Views used by Auth Controllers
	 * --------------------------------------------------------------------
	 *
	 * @var array
	 */
	public $views = [
		'login' => 'App\Views\Auth\login',
		'register' => 'App\Views\Auth\register',
		'forgot' => 'App\Views\Auth\forgot',
		'reset' => 'App\Views\Auth\reset',
		'emailForgot' => 'App\Views\Auth\emails\forgot',
		'emailActivation' => 'App\Views\Auth\emails\activation',
	];

	/**
	 * --------------------------------------------------------------------
	 * Layout for the views to extend
	 * --------------------------------------------------------------------
	 *
	 * @var string
	 */
	public $viewLayout = 'App\Views\Auth\layout';

	/**
	 * --------------------------------------------------------------------
	 * Additional Fields for "Nothing Personal"
	 * --------------------------------------------------------------------
	 *
	 * The `NothingPersonalValidator` prevents personal information from
	 * being used in passwords. The email and username fields are always
	 * considered by the validator. Do not enter those field names here.
	 *
	 * An extend User Entity might include other personal info such as
	 * first and/or last names. `$personalFields` is where you can add
	 * fields to be considered as "personal" by the NothingPersonalValidator.
	 *
	 * For example:
	 *	 $personalFields = ['firstname', 'lastname'];
	 *
	 * @var string[]
	 */
	public $personalFields = ['firstname', 'lastname'];

	/**
	 * --------------------------------------------------------------------
	 * Allow User Registration
	 * --------------------------------------------------------------------
	 *
	 * When enabled (default) any unregistered user may apply for a new
	 * account. If you disable registration you may need to ensure your
	 * controllers and views know not to offer registration.
	 *
	 * @var bool
	 */
	public $allowRegistration = false;

}