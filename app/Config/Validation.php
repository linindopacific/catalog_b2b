<?php

namespace Config;

use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;
use Myth\Auth\Authentication\Passwords\ValidationRules;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var string[]
	 */
	public $ruleSets = [
		Rules::class,
		FormatRules::class,
		FileRules::class,
		CreditCardRules::class,
		ValidationRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array<string, string>
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------

	public $itemPost = [
        "no" => "required|alpha_numeric|exact_length[8]",
		"description" => "required|string|max_length[255]",
		"description_2" => "permit_empty|string",
		"uom" => "required|string|max_length[20]",
		"cross_reference_no" => "required|string|max_length[50]",
		"item_category" => "required|string|max_length[50]",
		"product_group" => "required|string|max_length[50]",
		"manufacturer" => "required|string|max_length[50]",
		"weight" => "permit_empty|is_natural_no_zero",
		"image_cover" => "permit_empty|uploaded[image_cover]|is_image[image_cover]",
		"commodity" => "required|string|max_length[100]",
		"group_class" => "required|string|max_length[100]",
		"subgroup_one" => "required|string|max_length[100]",
		"subgroup_two" => "required|string|max_length[100]",
    ];

}