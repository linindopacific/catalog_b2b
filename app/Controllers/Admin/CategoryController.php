<?php

namespace App\Controllers\Admin;

/*
 * File: CategoryController.php
 * Project: -
 * File Created: Tuesday, 31st August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 2nd September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\BaseController;
use App\Libraries\Breadcrumb;

class CategoryController extends BaseController
{
	public function index()
	{
		$model = new \App\Models\ItemModel();
		$categories = $model->getCategories();

		return view("Admin/categories", [
            "title" => "Manage Categories",
            "categories" => $categories,
			"master" => "active",
			"master_category" => "active",
            "viewLayout" => "App\Views\Templates\Backend\layout"
        ]);
	}
}