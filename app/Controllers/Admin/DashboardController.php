<?php

namespace App\Controllers\Admin;

/*
 * File: DashboardController.php
 * Project: -
 * File Created: Thursday, 26th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 3rd September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\BaseController;
use Google\Analytics\Data\V1beta\BetaAnalyticsDataClient;
use Google\Analytics\Data\V1beta\DateRange;
use Google\Analytics\Data\V1beta\Dimension;
use Google\Analytics\Data\V1beta\Metric;

class DashboardController extends BaseController
{
    public function index()
    {
        /*
        $client = new BetaAnalyticsDataClient([
            "credentials" => FCPATH . "assets/keyfile.json",
        ]);
        $response = $client->runReport([
            "property" => "properties/285022144",
            "dateRanges" => [
                new DateRange([
                    "start_date" => "2021-09-01",
                    "end_date" => "today",
                ]),
            ],
            "dimensions" => [
                new Dimension([
                    "name" => "country",
                ]),
                new Dimension([
                    "name" => "browser"
                ]),
            ],
            "metrics" => [
                new Metric([
                    "name" => "activeUsers",
                ]),
                new Metric([
                    "name" => "newUsers",
                ]),
                new Metric([
                    "name" => "itemViews",
                ])
            ]
        ]);

        // Print results of an API call.
        echo 'Report result: ' . PHP_EOL;

        foreach ($response->getRows() as $row) {
            echo $row->getDimensionValues()[0]->getValue()
                . ' ' . $row->getDimensionValues()[1]->getValue()
                . ' ' . $row->getMetricValues()[0]->getValue() . PHP_EOL;

        }
        dd($response->serializeToJsonString());
        */

		return view("Admin/dashboard", [
            "title" => "Dashboard",
            "dashboard" => "active",
            "viewLayout" => "App\Views\Templates\Backend\layout"
        ]);
    }
}