<?php

namespace App\Controllers\Admin;

/*
 * File: ItemController.php
 * Project: -
 * File Created: Wednesday, 1st September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 14th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\BaseController;
use App\Entities\Item;
use App\Models\ItemModel;
use CodeIgniter\API\ResponseTrait;

class ItemController extends BaseController
{
	use ResponseTrait;

	public function index()
	{
		return view("Admin/items", [
            "title" => "Manage Items",
			"master" => "active",
			"master_item" => "active",
            "viewLayout" => "App\Views\Templates\Backend\layout"
        ]);
	}

	public function ssp()
	{
		if (! $this->request->isAJAX())
        {
            return $this->failForbidden();
        }

		// DB table to use
		$table = "items";

		// Table"s primary key
		$primaryKey = "id";

		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( "db" => "no", "dt" => 0 ),
			array( "db" => "cross_reference_no", "dt" => 1 ),
			array( "db" => "uom", "dt" => 2 ),
			array( "db" => "item_category", "dt" => 3 ),
			array( "db" => "manufacturer", "dt" => 4 ),
			array( "db" => "id", "dt" => 5, "formatter" => function( $d, $row ) {
				return anchor("admin/items/" . $d, "View");
			})
		);

		// SQL server connection information
		$db = db_connect();
		$sql_details = array(
			"user" => $db->username,
			"pass" => $db->password,
			"db"   => $db->database,
			"host" => $db->hostname
		);

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		* If you just want to use the basic configuration for DataTables with PHP
		* server-side, there is no need to edit below this line.
		*/
		return $this->respond(\App\Libraries\SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns ));
	}

	public function lookup(int $id)
	{
		$model = new ItemModel();

		if (! $data = $model->find($id))
		{
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		return view("Admin/item_view", [
			"title" => "Manage " . $data->no,
			"item" => $data,
			"master" => "active",
			"master_item" => "active",
			"viewLayout" => "App\Views\Templates\Backend\layout"
		]);
	}

	public function attemptPost()
	{
		if (! $this->validate("itemPost"))
		{
			return redirect()->back()->withInput()->with("errors", $this->validator->getErrors() ?? "bad attempt");
		}

		$itemModel = new ItemModel();
		//$itemModel->skipValidation(true); // we all already validate at first

		$id = $this->request->getPost("id");
		$data = $this->request->getPost();
		if (! empty($id))
		{
			$item = $itemModel->find($id);
		}
		else
		{
			$item = new Item();
		}

		$item->fill($data);
		if (! $itemModel->save($item))
		{
			return redirect()->back()->withInput()->with("errors", $itemModel->errors() ?? "bad attempt");
		}

		if ($imagefile = $this->request->getFile("image_cover"))
		{
			if ($imagefile->isValid() && ! $imagefile->hasMoved())
			{
				$newName = $imagefile->getRandomName();
				$uploadPath = WRITEPATH."uploads/products";
				if ($imagefile->move($uploadPath, $newName))
				{
					$publicPath = FCPATH . "assets/images/products";
					// large
					\Config\Services::image()
							->withFile($uploadPath . "/" . $newName)
							->save($publicPath . "/large/" . $newName, 100);
					// medium
					\Config\Services::image()
							->withFile($uploadPath . "/" . $newName)
							->resize(450, 327, false)
							->save($publicPath . "/medium/" . $newName, 100);
					// thumbnail
					\Config\Services::image()
							->withFile($uploadPath . "/" . $newName)
							->resize(280, 240, false)
							->save($publicPath . "/thumbnail/" . $newName, 100);
					// square_thumbnail
					\Config\Services::image()
							->withFile($uploadPath . "/" . $newName)
							->fit(69, 61)
							->save($publicPath . "/square_thumbnail/" . $newName, 100);

					$item->image_cover = $newName;
					$itemModel->save($item);
				}
			}
		}

		return redirect()->back()->withCookies()->with("success", "Successfully updated data.");
	}

	public function attemptPostApi()
	{
		if (! $this->validate("itemPost"))
		{
			return $this->failValidationErrors($this->validator->getErrors());
		}

		$itemModel = new ItemModel();

		$id = $this->request->getPost("id");
		$data = $this->request->getPost();
		$data["description_2"] = htmlspecialchars_decode($data["description_2"]);

		$item = $itemModel->find($id);
		if (! empty($item))
		{
			$item->fill($data);
			unset($data["id"]);
			if (! $itemModel->save($item))
			{
				log_message("error", print_r($itemModel->errors(), true));
				return $this->fail(	$itemModel->errors());
			}
		}
		else
		{
			$item = new Item();
			$item->fill($data);
			if (! $itemModel->insert($item))
			{
				log_message("error", print_r($itemModel->errors(), true));
				return $this->fail(	$itemModel->errors());
			}
		}

		if ($imagefile = $this->request->getFile("image_cover"))
		{
			if ($imagefile->isValid() && ! $imagefile->hasMoved())
			{
				$newName = $imagefile->getRandomName();
				$uploadPath = WRITEPATH . "uploads/products";
				if ($imagefile->move($uploadPath, $newName))
				{
					$publicPath = FCPATH . "assets/images/products";
					// large
					\Config\Services::image()
							->withFile($uploadPath . "/" . $newName)
							->save($publicPath . "/large/" . $newName, 100);
					// medium
					\Config\Services::image()
							->withFile($uploadPath . "/" . $newName)
							->resize(450, 327, false)
							->save($publicPath . "/medium/" . $newName, 100);
					// thumbnail
					\Config\Services::image()
							->withFile($uploadPath . "/" . $newName)
							->resize(280, 240, false)
							->save($publicPath . "/thumbnail/" . $newName, 100);
					// square_thumbnail
					\Config\Services::image()
							->withFile($uploadPath . "/" . $newName)
							->fit(69, 61)
							->save($publicPath . "/square_thumbnail/" . $newName, 100);

					$item->image_cover = $newName;
					if (! $itemModel->save($item))
					{
						log_message("error", print_r($itemModel->errors(), true));
					}
				}
			}
		}

		return $this->respond(["message" => "Successfully updated."]);
	}

	public function clear_file_images()
	{
		$db_images = [];
		$model = new ItemModel();
		foreach ($model->getFileImages() as $row)
		{
			$db_images[] = $row->image_cover;
		}

		helper("filesystem");
		$sourceDir = "./assets/images/products";
		$map = directory_map($sourceDir);
		foreach ($map as $key => $values)
		{
			foreach ($values as $value)
			{
				// if not exist, delete files
				if (! in_array($value, $db_images))
				{
					$key = str_replace('\\', '/', $key);
					$filePath = $sourceDir . '/' . $key . $value;

					if (! unlink($filePath))
					{
						log_message("error", $filePath . " tidak bisa dihapus.");
					}
				}
			}
		}

		return redirect()->route("dashboard");
	}

	public function delete()
	{
		$data = $this->request->getJSON(true);

        $validation =  \Config\Services::validation();
        $validation->setRules([
			"id" => "required"
		]);

        if (! $validation->run($data))
        {
            return $this->failValidationErrors($validation->getErrors());
        }

		$id = $data["id"];
		if (! is_array($id))
		{
			$id = [$id];
		}

		$itemModel = new ItemModel();
		if ($id[0] == 'all')
		{
			$itemModel->builder()->emptyTable();
			return $this->respond("OK");
		}

		if (! $itemModel->whereNotIn("id", $id)->delete())
		{
			return $this->fail($itemModel->errors());
		}

		$itemModel->purgeDeleted();
		return $this->respond("OK");
	}
}