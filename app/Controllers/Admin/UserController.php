<?php

namespace App\Controllers\Admin;

/*
 * File: UserController.php
 * Project: -
 * File Created: Friday, 27th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 31st August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\BaseController;
use App\Models\UserModel;

class UserController extends BaseController
{
	public function index()
	{
		$userModel = new UserModel();
		$users = $userModel->findAll();

		return view('Admin/users', [
            'title' => 'Manage Users',
            'users' => $users,
			'extras' => 'active',
			'extras_user' => 'active',
            'viewLayout' => 'App\Views\Templates\Backend\layout'
        ]);
	}
}
