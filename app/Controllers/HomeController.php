<?php

namespace App\Controllers;

/*
 * File: HomeController.php
 * Project: -
 * File Created: Thursday, 26th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 2nd September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\BaseController;

class HomeController extends BaseController
{
	public function frontpage()
	{
		$model = new \App\Models\ItemModel();
		$data = $model->orderBy("updated_at", "DESC")->findAll(8);

		return view("landing", [
			"title" => "Home",
			"products" => $data,
			"activeHome" => "active",
			"viewLayout" => "App\Views\Templates\Frontend\layout"
		]);
	}
}
