<?php

namespace App\Controllers;

/*
 * File: ProductController.php
 * Project: -
 * File Created: Monday, 30th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 22nd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\BaseController;
use App\Models\ItemModel;

class ProductController extends BaseController
{
	public function lookup(int $productId)
	{
		$model = new ItemModel();

		if (! $data = $model->find($productId))
		{
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		return view("product", [
			"title" => $data->no,
			"product" => $data,
			"activeProducts" => "active",
			"viewLayout" => "App\Views\Templates\Frontend\layout"
		]);
	}

	public function index()
	{
		$category = $this->request->getGet("category");
		$order = $this->request->getGet("order");
		$search = $this->request->getGet("search");

		$model1 = new ItemModel();
		$query = $model1->getList($category, $order, $search);

		$model2 = new ItemModel();
		$categories = $model2->getCategories();

		return view("products", [
			"title" => "Products",
			"products" => $query->paginate(6),
            "pager" => $query->pager,
			"search_query" => $search,
			"categories" => $categories,
			"order" => $order,
			"category" => $category,
			"activeProducts" => "active",
			"viewLayout" => "App\Views\Templates\Frontend\layout"
		]);
	}

	public function pdf(int $productId)
	{
		$model = new ItemModel();

		if (! $data = $model->find($productId))
		{
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$mpdf = new \Mpdf\Mpdf([
			"mode" => "utf-8",
			"tempDir" => WRITEPATH . "temp"
		]);

		$mpdf->setLogger(new class extends \Psr\Log\AbstractLogger {
			public function log($level, $message, array $context = [])
			{
				log_message($level, $message);
			}
		});

		$mpdf->SetTitle($data->no);
		$mpdf->SetAuthor(env("APP_NAME"));
		$mpdf->SetCreator("MIS7102");
		$mpdf->SetKeywords($data->description);

		$mpdf->imageVars['product-img'] = file_get_contents($data->image_thumbnail);

		$html = "<table style='page-break-inside:avoid' autosize='1'>";
		// product head
		$html.= "<tr>
					<td rowspan='6'><img src='var:product-img' /></td>
					<td><strong><i>part number</i></strong></td>
				</tr>
				<tr>
					<td>" . $data->no . "</td>
				</tr>
				<tr>
					<td><strong><i>product code</i></strong></td>
				</tr>
				<tr>
					<td>" . $data->cross_reference_no . "</td>
				</tr>
				<tr>
					<td><strong><i>description</i></strong></td>
				</tr>
				<tr>
					<td>" . $data->description . "</td>
				</tr>";
		// blank
		$html.= "<tr>
					<td colspan='2'></td>
				</tr>";
		// product information
		$html.= "<tr>
					<td colspan='2'><strong><i>more information</i></strong></td>
				</tr>
				<tr>
					<td colspan='2'>" . $data->description_2 . "</td>
				</tr>";
		// blank
		$html.= "<tr>
					<td colspan='2'></td>
				</tr>
				<tr>
					<td colspan='2'></td>
				</tr>";
		// product specification
		$html.= "<tr>
					<td colspan='2'><strong><i>specifications</i></strong></td>
				</tr>
				<tr>
					<td colspan='2'>
						<p>Sales Unit of Measure: " . $data->uom . "</p>
						<p>Weight: " . $data->weight . " kg</p>
					</td>
				</tr>";
		$html.= "</table>";

		$mpdf->simpleTables = true;
		$mpdf->setHTMLFooter(site_url('products/' . $data->id));
		$mpdf->WriteHTML($html);

		return $mpdf->Output("filename.pdf", \Mpdf\Output\Destination::DOWNLOAD);
	}

}