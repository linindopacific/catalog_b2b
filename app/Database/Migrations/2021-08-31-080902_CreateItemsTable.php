<?php

namespace App\Database\Migrations;

/*
 * File: 2021-08-31-080902_CreateItemsTable.php
 * Project: -
 * File Created: Tuesday, 31st August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 16th September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Database;
use CodeIgniter\Database\Migration;

class CreateItemsTable extends Migration
{
	protected $tbl_items = "items";

	protected $tbl_properties = "items_properties";

	public function up()
	{
		// Table items
		$fields = [
            "id" => [
				"type" => "bigint",
				"unsigned" => true,
				"auto_increment" => true
			],
            "no" => [
				"type" => "varchar",
				"constraint" => 10,
				"null" => false
			],
			"description" => [
				"type" => "varchar",
				"constraint" => 255,
				"null" => false
			],
			"uom" => [
				"type" => "varchar",
				"constraint" => 20,
				"null" => false
			],
			"cross_reference_no" => [
				"type" => "varchar",
				"constraint" => 50,
				"null" => false
			],
			"item_category" => [
				"type" => "varchar",
				"constraint" => 50,
				"null" => true
			],
			"product_group" => [
				"type" => "varchar",
				"constraint" => 50,
				"null" => true
			],
			"manufacturer" => [
				"type" => "varchar",
				"constraint" => 50,
				"null" => true
			],
			"commodity" => [
                "type" => "varchar",
                "constraint" => 100,
                "null" => true
            ],
            "group_class" => [
                "type" => "varchar",
                "constraint" => 100,
                "null" => true
            ],
            "subgroup_one" => [
                "type" => "varchar",
                "constraint" => 100,
                "null" => true
            ],
            "subgroup_two" => [
                "type" => "varchar",
                "constraint" => 100,
                "null" => true
			],
			"weight" => [
				"type" => "decimal(38,20)",
				"default" => 0
			],
			"featured" => [
				"type" => "TINYINT",
				"null" => false,
				"default" => 0
			],
			"image_cover" => [
				"type" => "TEXT",
				"null" => true,
			],
			"created_at" => [
				"type" => "datetime",
				"null" => true
			],
            "updated_at" => [
				"type" => "datetime",
				"null" => true
			],
            "deleted_at" => [
				"type" => "datetime",
				"null" => true
			]
        ];
		$this->forge->addField($fields);
        $this->forge->addKey("id", true);
		$this->forge->addKey(["no", "cross_reference_no"], false, true);
		$this->forge->createTable($this->tbl_items, true);

		// Table items_properties
		$fields = [
            "id" => [
				"type" => "bigint",
				"unsigned" => true,
				"auto_increment" => true
			],
            "key" => [
				"type" => "varchar",
				"constraint" => 50,
				"null" => false
			],
			"value" => [
				"type" => "text",
				"null" => false
			],
			"items_id" => [
				"type" => "bigint",
				"unsigned" => true
			],
			"created_at" => [
				"type" => "datetime",
				"null" => true
			],
            "updated_at" => [
				"type" => "datetime",
				"null" => true
			],
            "deleted_at" => [
				"type" => "datetime",
				"null" => true
			]
        ];
		$this->forge->addField($fields);
        $this->forge->addKey("id", true);
		$this->forge->addForeignKey("items_id", $this->tbl_items, "id", "CASCADE", "CASCADE");
		$this->forge->createTable($this->tbl_properties, true);
	}

	public function down()
	{
		// backup first
		$this->_backup($this->tbl_items);
		$this->_backup($this->tbl_properties);

		// drop item table
		$this->forge->dropTable($this->tbl_items, true);
		// drop item properties table
		$this->forge->dropTable($this->tbl_properties, true);
	}

	function _backup($table)
	{
		$db = db_connect($this->getDBGroup());
		$builder = $db->table($table);

		$util = (new Database())->loadUtils($db);
		$data = $util->getCSVFromResult($builder->get());

		helper("filesystem");
		$filename = $table . "_" . time() . ".csv";
		if (! write_file(WRITEPATH . "dbdump/" . $filename, $data))
		{
			log_message("error", "Unable to write the backup file");
			die("Unable to write the backup file");
		}
	}
}
