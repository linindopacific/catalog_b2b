<?php

namespace App\Database\Migrations;

/*
 * File: 2021-10-11-071436_AlterItemsTableMoreInfo.php
 * Project: Migrations
 * File Created: Monday, 11th October 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 11th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Database;
use CodeIgniter\Database\Migration;

class AlterItemsTableMoreInfo extends Migration
{
    protected $table = "items";

    public function up()
    {
        $fields = [
            "description_2" => [
                "type" => "text",
                "null" => true,
                "after" => "description"
            ]
        ];

        $this->forge->addColumn($this->table, $fields);
    }

    public function down()
    {
        // backup first
		$this->_backup();

		$this->forge->dropColumn($this->table, ["description_2"]);
    }

    function _backup()
	{
		$db = db_connect($this->getDBGroup());
		$builder = $db->table($this->table);

		$util = (new Database())->loadUtils($db);
		$data = $util->getCSVFromResult($builder->get());

		helper("filesystem");
		$filename = $this->table . "_" . time() . ".csv";
		if (! write_file(WRITEPATH . "dbdump/" . $filename, $data))
		{
			log_message("error", "Unable to write the backup file");
			die("Unable to write the backup file");
		}
	}
}