<?php

namespace App\Database\Seeds;

/*
 * File: ItemSeeder.php
 * Project: -
 * File Created: Tuesday, 31st August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 3rd September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class ItemSeeder extends Seeder
{
	protected $tbl_items = 'items';

	protected $tbl_properties = 'items_properties';

	public function run()
	{
		$path = WRITEPATH . 'uploads/items.csv';
		$file = new \CodeIgniter\Files\File($path);

		if ($file->isReadable())
		{
			$csv = $file->openFile();
			$row = 0;
			$itemModel = new \App\Models\ItemModel();
			while (! $csv->eof())
			{
				$line = $csv->fgets();
				$line_arr = explode(';', $line);

				if ($row > 0)
				{
					$itemNo = trim($line_arr[0]);
					$uom = trim($line_arr[1]);
					$crossRefNo = trim($line_arr[2]);
					$description = trim($line_arr[3]);
					$itemCategory = trim($line_arr[4]);
					$productGroup = trim($line_arr[5]);
					$manufacturer = trim($line_arr[6]);
					$weight = trim($line_arr[7]);

					// item
					$data = [
						'no' => $itemNo,
						'description' => $description,
						'uom' => $uom,
						'cross_reference_no' => $crossRefNo,
						'item_category' => ($itemCategory == '\0') ? NULL : $itemCategory,
						'product_group' => ($productGroup == '\0') ? NULL : $productGroup,
						'manufacturer' => ($manufacturer == '\0') ? NULL : $manufacturer,
						'weight' => ($weight == '\0') ? 0 : floatval(str_replace(",",".",$weight))
					];

					$validation =  \Config\Services::validation();
					if (! $result = $validation->run($data, 'itemCreate'))
					{
						log_message('error', implode(' ', $validation->getErrors()));
						die(implode(' ', $validation->getErrors()));
					}
					else
					{
						die(var_dump($result));

						if (! $itemModel->save($data))
						{
							log_message('error', implode(' ', $itemModel->errors()));
							die(implode(' ', $itemModel->errors()));
						}
					}
				}

				$row++;
			}
		}
	}
}
