<?php

namespace App\Entities;

/*
 * File: Item.php
 * Project: -
 * File Created: Tuesday, 31st August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 22nd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Models\ItemModel;
use CodeIgniter\Entity\Entity;

class Item extends Entity
{
	protected $datamap = [];

	protected $dates = [
		"created_at",
		"updated_at",
		"deleted_at",
	];

	protected $casts = [
		"weight" => "float",
		"featured" => "boolean"
	];

	public function getUrl()
	{
		// got problem with "\" & "." like A935H-L/9004-12V
		return site_url("products/". $this->attributes["id"]);
	}

	public function getDetail()
	{
		if (! empty($this->attributes["description"]))
		{
			return $this->attributes["description"];
		}
		else
		{
			return "";
		}
	}

	public function getWeight()
	{
		return round($this->attributes["weight"], 0, PHP_ROUND_HALF_UP);
	}

	public function getImageSquareThumbnail()
	{
		if ($image_cover = $this->attributes["image_cover"])
		{
			$path = "assets/images/products/square_thumbnail/" . $image_cover;
			if ($this->isImageExists($path))
			{
				$imgUrl = base_url($path);
			}
			else
			{
				$imgUrl = "https://via.placeholder.com/69x61/ffffff/90c322.png?text=Image+Not+Found";
			}
		}
		else
		{
			$imgUrl = "https://via.placeholder.com/69x61/ffffff/90c322.png?text=Item";
		}

		return $imgUrl;
	}

	public function getImageThumbnail()
	{
		if ($image_cover = $this->attributes["image_cover"])
		{
			$path = "assets/images/products/thumbnail/" . $image_cover;
			if ($this->isImageExists($path))
			{
				$imgUrl = base_url($path);
			}
			else
			{
				$imgUrl = "https://via.placeholder.com/280x240/ffffff/90c322.png?text=Image+Not+Found";
			}
		}
		else
		{
			$imgUrl = "https://via.placeholder.com/280x240/ffffff/90c322.png?text=";
		}

		return $imgUrl;
	}

	public function getImageMedium()
	{
		if ($image_cover = $this->attributes["image_cover"])
		{
			$path = "assets/images/products/medium/" . $image_cover;
			if ($this->isImageExists($path))
			{
				$imgUrl = base_url($path);
			}
			else
			{
				$imgUrl = "https://via.placeholder.com/450x327/ffffff/90c322.png?text=Image+Not+Found";
			}
		}
		else
		{
			$imgUrl = "https://via.placeholder.com/450x327/ffffff/90c322.png?text=";
		}

		return $imgUrl;
	}

	public function getImageLarge()
	{
		if ($image_cover = $this->attributes["image_cover"])
		{
			$path = "assets/images/products/large/" . $image_cover;
			if ($this->isImageExists($path))
			{
				$imgUrl = base_url($path);
			}
			else
			{
				$imgUrl = "https://via.placeholder.com/1280x720/ffffff/90c322.png?text=Image+Not+Found";
			}
		}
		else
		{
			$imgUrl = "https://via.placeholder.com/1280x720/ffffff/90c322.png?text=";
		}

		return $imgUrl;
	}

	private function isImageExists(string $path) : bool
	{
		$path = FCPATH  . "/" . $path;

		try {
			$file = new \CodeIgniter\Files\File($path, true);
		}
		catch (\Exception $e) {
			log_message("error", $e->getMessage());
			return false;
		}

		return true;
	}

	public function getRelated()
	{
		$model = new ItemModel();

		if ($key = $this->attributes["subgroup_two"])
		{
			$model->where("subgroup_two", $key);
		}
		elseif ($key = $this->attributes["subgroup_one"])
		{
			$model->where("subgroup_one", $key);
		}
		elseif ($key = $this->attributes["group_class"])
		{
			$model->where("group_class", $key);
		}
		elseif ($key = $this->attributes["commodity"])
		{
			$model->where("commodity", $key);
		}
		elseif ($key = $this->attributes["product_group"])
		{
			$model->where("product_group", $key);
		}
		elseif ($key = $this->attributes["item_category"])
		{
			$model->where("item_category", $key);
		}
		else
		{
			$model->where("SUBSTRING(no, 1, 4)", substr($this->attributes["no"], 0, 4));
		}

		return $model->where("id !=", $this->attributes["id"])
					 ->orderBy("cross_reference_no", "RANDOM")
					 ->findAll(4);
	}

	public function setNo(string $no)
	{
		$this->attributes["no"] = strtoupper($no);

        return $this;
	}

	public function setWeight(string $weight)
	{
		$this->attributes["weight"] = floatval($weight);

        return $this;
	}

	public function setDescription2(string $description)
	{
		$this->attributes["description_2"] = htmlspecialchars($description);

        return $this;
	}

	public function getDescription2()
	{
		return htmlspecialchars_decode($this->attributes["description_2"]);
	}

}