<?php

namespace App\Models;

/*
 * File: ItemModel.php
 * Project: Models
 * File Created: Tuesday, 31st August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 27th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\Item;
use CodeIgniter\Model;

class ItemModel extends Model
{
	protected $table = "items";

	protected $returnType = Item::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
		"id", "no", "description", "description_2", "uom", "cross_reference_no", "item_category", "product_group", "manufacturer",
		"weight", "image_cover", "commodity", "group_class", "subgroup_one", "subgroup_two", "featured"
	];

	protected $useTimestamps = true;

	public function getList($category, $order, $search)
	{
		$this->where("manufacturer !=", NULL);
		if ($search)
		{
			$this->like("no", $search);
			$this->orlike("description", $search);
			$this->orlike("description_2", $search);
			$this->orlike("cross_reference_no", $search);
			$this->orlike("manufacturer", $search);
			$this->orlike("subgroup_two", $search);
			$this->orlike("subgroup_one", $search);
		}
		if ($category)
		{
			$this->where("subgroup_one", $category);
		}

		$order = $order ?? "asc";
		return $this->orderBy("no", $order);
	}

	public function getCategories()
	{
		$builder = $this->builder();
		$query = $builder->select("subgroup_one as name, COUNT(*) as items")
						 ->where("subgroup_one <>", "")
						 ->groupBy("subgroup_one")
						 ->get();

		return $query->getResult();
	}

	public function getFileImages()
	{
		$builder = $this->builder();
		$query = $builder->select("image_cover")
						 ->where("image_cover <>", NULL)
						 ->get();

		return $query->getResult();
	}

}