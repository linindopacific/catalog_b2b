<?php

namespace App\Models;

/*
 * File: ItemPropertyModel.php
 * Project: -
 * File Created: Tuesday, 31st August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 31st August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\ItemProperty;
use CodeIgniter\Model;

class ItemPropertyModel extends Model
{
	protected $table = 'items_properties';

	protected $returnType = ItemProperty::class;

	protected $allowedFields = [
		'key', 'value', 'items_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'key' => 'required|alpha|max_length[50]',
        'value' => 'required|max_length[255]',
        'items_id' => 'required|integer',
	];

	protected $validationMessages = [];

}