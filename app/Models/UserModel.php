<?php

namespace App\Models;

/*
 * File: UserModel.php
 * Project: -
 * File Created: Thursday, 26th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 26th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\User;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Myth\Auth\Models\UserModel as MythModel;

class UserModel extends MythModel
{
    protected $allowedFields = [
        'email', 'username', 'password_hash', 'reset_hash', 'reset_at', 'reset_expires', 'activate_hash',
        'status', 'status_message', 'active', 'force_pass_reset', 'permissions', 'deleted_at',
        'firstname', 'lastname', 'phone',
    ];

    protected $returnType = User::class;

    /**
     * Generate a random password
     *
     * @return string
     */
    public function get_random_password()
    {
        $config = new \Config\Auth();

        $generator = new ComputerPasswordGenerator();
        $generator
            ->setUppercase()
            ->setLowercase()
            ->setNumbers()
            ->setSymbols(false)
            ->setLength($config->minimumPasswordLength);

        return $generator->generatePasswords()[0];
    }
}