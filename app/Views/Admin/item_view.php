<?= $this->extend($viewLayout) ?>

<?= $this->section("pageStyles") ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<?= $this->endSection() ?>

<?= $this->section("main") ?>
<div class="col-12 col-lg-4">
    <div class="card">
        <?= anchor($item->image_large, img($item->image_medium, false, ["class" => "card-img-top", "alt" => "", "id" => "img_item"]), ["id" => "img_item_link", "class" => "img-popup-link"]); ?>
    </div>
</div>
<div class="col-12 col-lg-8">
    <?= form_open_multipart("admin/items", ["class" => "row g-3"]); ?>
        <?= form_hidden("id", $item->id); ?>
        <div class="col-12 col-lg-10">
            <div class="mb-3">
                <label for="candeluxNo" class="form-label">Candelux No.</label>
                <input type="text" class="form-control <?= (session("errors.no")) ? "is-invalid" : ""; ?>" id="candeluxNo" name="no" value="<?= old("no", $item->no) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.no") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="crossRefNo" class="form-label">Cross-Reference No.</label>
                <input type="text" class="form-control <?= (session("errors.cross_reference_no")) ? "is-invalid" : ""; ?>" id="crossRefNo" name="cross_reference_no" value="<?= old("cross_reference_no", $item->cross_reference_no) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.cross_reference_no") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="description" class="form-label">Description</label>
                <input type="text" class="form-control <?= (session("errors.description")) ? "is-invalid" : ""; ?>" id="description" name="description" value="<?= old("description", $item->description) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.description") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="description2" class="form-label">More Information</label>
                <textarea class="form-control <?= (session("errors.description_2")) ? "is-invalid" : ""; ?>" id="description2" name="description_2"><?= old("description_2", $item->description_2) ?></textarea>
                <div class="invalid-feedback">
                    <?= session("errors.description_2") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="uom" class="form-label">Unit of Measure</label>
                <input type="text" class="form-control <?= (session("errors.uom")) ? "is-invalid" : ""; ?>" id="uom" name="uom", value="<?= old("uom", $item->uom) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.uom") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="itemCategory" class="form-label">Item Category</label>
                <input type="text" class="form-control <?= (session("errors.item_category")) ? "is-invalid" : ""; ?>" id="itemCategory" name="item_category" value="<?= old("item_category", $item->item_category) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.item_category") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="productGroup" class="form-label">Product Group</label>
                <input type="text" class="form-control <?= (session("errors.product_group")) ? "is-invalid" : ""; ?>" id="productGroup" name="product_group" value="<?= old("product_group", $item->product_group) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.product_group") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="manufacturer" class="form-label">Manufacturer</label>
                <input type="text" class="form-control <?= (session("errors.manufacturer")) ? "is-invalid" : ""; ?>" id="manufacturer" name="manufacturer" value="<?= old("manufacturer", $item->manufacturer) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.manufacturer") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="commodity" class="form-label">Commodity</label>
                <input type="text" class="form-control <?= (session("errors.commodity")) ? "is-invalid" : ""; ?>" id="commodity" name="commodity" value="<?= old("commodity", $item->commodity) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.commodity") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="group_class" class="form-label">Group Class</label>
                <input type="text" class="form-control <?= (session("errors.group_class")) ? "is-invalid" : ""; ?>" id="group_class" name="group_class" value="<?= old("group_class", $item->group_class) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.group_class") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="subgroup_one" class="form-label">SubGroup One</label>
                <input type="text" class="form-control <?= (session("errors.subgroup_one")) ? "is-invalid" : ""; ?>" id="subgroup_one" name="subgroup_one" value="<?= old("subgroup_one", $item->subgroup_one) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.subgroup_one") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="subgroup_two" class="form-label">SubGroup Two</label>
                <input type="text" class="form-control <?= (session("errors.subgroup_two")) ? "is-invalid" : ""; ?>" id="subgroup_two" name="subgroup_two" value="<?= old("subgroup_two", $item->subgroup_two) ?>" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.subgroup_two") ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="weight" class="form-label">Weight</label>
                <input type="number" class="form-control <?= (session("errors.weight")) ? "is-invalid" : ""; ?>" id="weight" name="weight" value="<?= old("weight", $item->weight) ?>" min="0" readonly>
                <div class="invalid-feedback">
                    <?= session("errors.weight") ?>
                </div>
            </div>
            <div class="mb-3">
                <?=anchor('admin/items', 'Back', ["class" => "btn btn-secondary"]) ?>
                <input type="button" class="btn btn-primary" style="visibility:hidden;" />
            </div>
        </div>
    <?= form_close(); ?>
</div>
<?= $this->endSection() ?>

<?= $this->section("pageScripts") ?>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        tinymce.init({
            selector: '#description2',
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
            readonly : 1
        });

        $(".img-popup-link").magnificPopup({
            type: "image"
        });
    });
</script>
<?= $this->endSection() ?>