<?= $this->extend($viewLayout) ?>

<?= $this->section("pageStyles") ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.0/css/dataTables.bootstrap5.min.css"><link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.9/css/fixedHeader.bootstrap5.min.css">
<style>
    thead input {
        width: 100%;
    }
</style>
<?= $this->endSection() ?>

<?= $this->section("main") ?>
<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped" id="table1" style="width: 100%;">
                <thead>
                    <tr>
                        <th>Item No.</th>
                        <th>Cross. Ref. No.</th>
                        <th>UoM</th>
                        <th>Category</th>
                        <th>Manufacturer</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section("pageScripts") ?>
<script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.0/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.9/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var url = "<?= route_to("admin/items/ssp"); ?>";

        // Setup - add a text input to each footer cell
        $('#table1 thead tr').clone(true).addClass('filters').appendTo('#table1 thead');

        var table = $('#table1').DataTable({
            dom: 'lrtip',
            orderCellsTop: true,
            fixedHeader: false,
            processing: true,
            serverSide: true,
            ajax: url
        });

        table.columns().eq(0).each(function(colIdx) {
            var cell = $('.filters th').eq($(table.column(colIdx).header()).index());
            var title = $(cell).text();
            $(cell).html( '<input type="text" placeholder="Search '+title+'" />' );

            $('input', $('.filters th').eq($(table.column(colIdx).header()).index()) ).off('keyup change').on('keyup change', function (e) {
                e.stopPropagation();
                $(this).attr('title', $(this).val());
                var regexr = '{search}'; //$(this).parents('th').find('select').val();
                table
                    .column(colIdx)
                    .search((this.value != "") ? regexr.replace('{search}', this.value) : "", this.value != "", this.value == "")
                    .draw();
            });

            $('select', $('.filters th').eq($(table.column(colIdx).header()).index()) ).off('change').on('change', function () {
                $(this).parents('th').find('input').trigger('change');
            });
        });

        $('#table1 tbody').on( 'click', 'button', function () {
            var data = table.row( $(this).parents('tr') ).data();
            alert( data[0] +"'s is: "+ data[ 5 ] );
        });

    } );
</script>
<?= $this->endSection() ?>