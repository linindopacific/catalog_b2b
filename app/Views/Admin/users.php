<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.0/css/dataTables.bootstrap5.min.css">
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped" id="table1" style="width: 100%;">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Created At</th>
                        <th style="text-align: center;">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user): ?>
                    <tr>
                        <td><?= $user->email; ?></td>
                        <td><?= $user->name; ?></td>
                        <td><?= $user->created_at->toDateString(); ?></td>
                        <td style="text-align: center;">
                            <span class="badge <?= ($user->active) ? 'bg-success' : 'bg-danger'; ?>"><?= ($user->active) ? 'Active' : 'Inactive'; ?></span>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.0/js/dataTables.bootstrap5.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#table1').DataTable();
    } );
</script>
<?= $this->endSection() ?>