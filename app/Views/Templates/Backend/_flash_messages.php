<div class="col-12">
    <?php if(session("success")): ?>
        <div class="alert alert-success" role="alert"><?= session("success.message") ?></div>
    <?php endif; ?>
    <?php if(session("info")): ?>
        <div class="alert alert-info" role="alert"><?= session("info.message") ?></div>
    <?php endif; ?>
</div>
