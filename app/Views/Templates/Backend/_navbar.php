<nav class="main-navbar">
    <div class="container">
        <ul>
            <li class="menu-item <?= ($dashboard) ?? ""; ?>">
                <a href="<?= site_url("admin/dashboard") ?>" class="menu-link">
                    <i class="bi bi-grid-fill"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="menu-item <?= ($master) ?? ""; ?> has-sub">
                <a href="#" class="menu-link">
                    <i class="bi bi-table"></i>
                    <span>Master</span>
                </a>
                <div class="submenu ">
                    <div class="submenu-group-wrapper">
                        <ul class="submenu-group">
                            <li class="submenu-item <?= ($master_item) ?? ""; ?>">
                                <?= anchor("admin/items", "Item", ["class" => "submenu-link"]) ?>
                            </li>
                            <li class="submenu-item <?= ($master_category) ?? ""; ?>">
                                <?= anchor("admin/category", "Category", ["class" => "submenu-link"]) ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="menu-item <?= ($extras) ?? ""; ?> has-sub">
                <a href="#" class="menu-link">
                    <i class="bi bi-plus-square-fill"></i>
                    <span>Extras</span>
                </a>
                <div class="submenu ">
                    <div class="submenu-group-wrapper">
                        <ul class="submenu-group">
                            <li class="submenu-item <?= ($extras_user) ?? ""; ?>">
                                <?= anchor("admin/users", "User", ["class" => "submenu-link"]) ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>