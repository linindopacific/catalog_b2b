<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?? ''; ?> &middot; <?= env('APP_NAME'); ?></title>
    <meta name="description" content="">
    <meta name="author" content="MIS7102">
    <!-- Css -->
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/perfect-scrollbar/1.5.2/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/vendors/bootstrap-icons/bootstrap-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/vendors/iconly/bold.css'); ?> ">
    <link rel="icon" href="<?= base_url('favicon.ico'); ?>">

    <?= $this->renderSection('pageStyles'); ?>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
    <div id="app">
        <div id="main" class="layout-horizontal">
            <header class="mb-5">
                <div class="header-top">
                    <div class="container">
                        <div class="logo">
                            <a href="<?= route_to('frontpage'); ?>">
                                <img src="<?= base_url('assets/images/logo/logo.png'); ?>" alt="Logo" srcset="">
                            </a>
                        </div>
                        <div class="header-top-right">
                            <!-- Burger button responsive -->
                            <a href="#" class="burger-btn d-block d-xl-none">
                                <i class="bi bi-justify fs-3"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <?= view('App\Views\Templates\Backend\_navbar'); ?>
            </header>
            <div class="content-wrapper container">
                <div class="page-heading">
                    <div class="page-title">
                        <div class="row">
                            <div class="col-12 col-md-6 order-md-1 order-last">
                                <h3><?= $title ?? ''; ?></h3>
                                <p class="text-subtitle text-muted"><?= $subtitle ?? ''; ?></p>
                            </div>
                            <div class="col-12 col-md-6 order-md-2 order-first"><?= ($breadcrumb) ?? ''; ?></div>
                        </div>
                    </div>
                </div>
                <div class="page-content">
                    <section class="row">
                        <?= view('App\Views\Templates\Backend\_flash_messages'); ?>
                        <?= $this->renderSection('main'); ?>
                    </section>
                </div><!-- /.page-content -->
            </div>
            <?= view('App\Views\Templates\Backend\_footer'); ?>
        </div>
    </div>
    <!-- jS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/perfect-scrollbar/1.5.2/perfect-scrollbar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets/js/pages/horizontal-layout.js'); ?>"></script>

    <?= $this->renderSection('pageScripts'); ?>
</body>
</html>