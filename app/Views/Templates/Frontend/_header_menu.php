<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div> <!-- End of /.navbar-header -->

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav nav-main">
                <li class="<?= $activeHome ?? ''; ?>">
                    <a href="<?= route_to('frontpage'); ?>">HOME</a>
                </li>
                <li class="<?= $activeProducts ?? ''; ?>">
                    <a href="<?= route_to('products'); ?>">PRODUCTS</a>
                </li>
                <li class="">
                    <a href="#">ABOUT US</a>
                </li>
                <!--li class="dropdown">
                    <a href="#">
                        PAGES
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                    </ul>
                </li-->
                <!-- End of /.dropdown -->
            </ul> <!-- End of /.nav-main -->
        </div>	<!-- /.navbar-collapse -->
    </div>	<!-- /.container-fluid -->
</nav>	<!-- End of /.nav -->