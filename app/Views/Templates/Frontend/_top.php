<section id="top">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <p class="contact-action"></p>
            </div>
            <div class="col-md-3 clearfix">
                <ul class="login-cart">
                    <li>
                        <?php
                            if (! logged_in())
                            {
                                echo anchor('login', '<i class="fa fa-user"></i> Login');
                            }
                            else
                            {
                                echo anchor('#', '<i class="fa fa-user"></i> ' . user()->name);
                            }
                        ?>
                    </li>
                </ul>
            </div>
            <div class="col-md-2">
                <div class="search-box">
                    <?= form_open('products', ['method' => 'get']); ?>
                    <div class="input-group">
                        <input placeholder="Search Here" type="text" name="search" class="form-control" required value="<?= ($search_query) ?? ''; ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"></button>
                        </span>
                    </div><!-- /.input-group -->
                    <?= form_close(); ?>
                </div><!-- /.search-box -->
            </div>
        </div> <!-- End Of /.row -->
    </div>	<!-- End Of /.Container -->
</section>