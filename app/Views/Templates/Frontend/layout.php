<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title><?= $title ?? ''; ?> &middot; <?= env('APP_NAME'); ?></title>
    <meta name="description" content="">
    <meta name="author" content="MIS7102">
	<!-- Css -->
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        :root {
	        --main-color: <?= env("APP_COLOR") ?>;
        }
    </style>
	<link rel="stylesheet" href="<?= base_url('assets/css/style.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/responsive.css'); ?>">
    <link rel="icon" href="<?= base_url('favicon.ico'); ?>">
    <?= $this->renderSection('pageStyles'); ?>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
    <!-- TOP HEADER Start
    ================================================== -->
    <?= view('App\Views\Templates\Frontend\_top'); ?>
    <!-- LOGO Start
    ================================================== -->
    <?= view('App\Views\Templates\Frontend\_header_logo'); ?>
    <!-- MENU Start
    ================================================== -->
    <?= view('App\Views\Templates\Frontend\_header_menu'); ?>

    <?= $this->renderSection('main'); ?>

    <!-- FOOTER Start
    ================================================== -->
    <?= view('App\Views\Templates\Frontend\_footer'); ?>
    <a id="back-top" href="#"></a>
	<!-- jS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/scrollup/2.4.1/jquery.scrollUp.min.js"></script>
	<script src="<?= base_url('assets/js/main.js'); ?>" type="text/javascript"></script>
	<?= $this->renderSection('pageScripts'); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-QH3B72E3L7"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '<?= env('GTAG') ?>');
    </script>
</body>
</html>