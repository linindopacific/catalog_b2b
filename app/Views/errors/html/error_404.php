<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404 - <?= env('APP_NAME'); ?></title>
	<meta name="description" content="">
    <meta name="author" content="MIS7102">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/vendors/bootstrap-icons/bootstrap-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">
	<style {csp-style-nonce}>
		#error {
			height: 100vh;
			background-color: #ebf3ff;
			padding-top: 5rem;
		}
		#error .img-error {
			width: 100%;
		}
		#error .error-title {
			font-size: 4rem;
			margin-top: 3rem;
		}
	</style>
</head>
<body>
    <div id="error">
		<div class="error-page container">
			<div class="col-md-8 col-12 offset-md-2">
				<div class="text-center">
					<h1 class="error-title">NOT FOUND</h1>
					<p class='fs-5 text-gray-600'>
						<?php if (! empty($message) && $message !== '(null)') : ?>
							<?= nl2br(esc($message)) ?>
						<?php else : ?>
							Sorry! Cannot seem to find the page you were looking for.
						<?php endif ?>
					</p>
					<a href="<?= base_url(); ?>" class="btn btn-lg btn-outline-primary mt-3">Go Home</a>
				</div>
			</div>
		</div>
    </div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/js/bootstrap.bundle.min.js"></script>
</body>
</html>