<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nivoslider/3.2/nivo-slider.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<!-- SLIDER Start
================================================== -->
<section id="slider-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="slider" class="nivoSlider">
                    <img src="https://via.placeholder.com/1180x500.png?text=Slider+1" alt="" />
                    <img src="https://via.placeholder.com/1180x500.png?text=Slider+2" alt=""/>
                    <img src="https://via.placeholder.com/1180x500.png?text=Slider+3" alt="" />
                </div>	<!-- End of /.nivoslider -->
            </div>	<!-- End of /.col-md-12 -->
        </div>	<!-- End of /.row -->
    </div>	<!-- End of /.container -->
</section> <!-- End of Section -->

<?php /*
<!-- FEATURES Start
================================================== -->

<section id="features">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="block">
                    <div class="media">
                        <div class="pull-left" href="#">
                            <i class="fa fa-ambulance"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Free Shipping</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="block">
                    <div class="media">
                        <div class="pull-left" href="#">
                            <i class=" fa fa-foursquare"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Media heading</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="block">
                    <div class="media">
                        <div class="pull-left" href="#">
                            <i class=" fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Call Us</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                        </div>	<!-- End of /.media-body -->
                    </div>	<!-- End of /.media -->
                </div>	<!-- End of /.block -->
            </div> <!-- End of /.col-md-4 -->
        </div>	<!-- End of /.row -->
    </div>	<!-- End of /.container -->
</section>	<!-- End of section -->
*/ ?>

<!-- PRODUCTS Start
================================================== -->
<section id="products">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="products-heading">
                    <h2>NEW PRODUCTS</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach($products as $product): ?>
                <div class="col-md-3">
                    <div class="products">
                        <?= anchor($product->url, img($product->image_thumbnail, false, ["alt" => ""])); ?>
                        <h4><?= $product->no; ?></h4>
                        <p class="crossref"><?= $product->cross_reference_no; ?></p>
                        <a class="view-link shutter" href="<?= $product->url; ?>">
                            <i class="fa fa-phone-square"></i>Contact Us
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>	<!-- End of /.row -->
    </div>	<!-- End of /.container -->
</section>	<!-- End of Section -->

<!-- CALL TO ACTION Start
================================================== -->
<section id="call-to-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <div class="block-heading">
                        <h2>Our Partners</h2>
                    </div>
                </div>	<!-- End of /.block -->
                <div id="owl-example" class="owl-carousel">
                    <div> <img src="https://via.placeholder.com/200x80/ffffff/90c322.png?text=client+1" alt=""></div>
                    <div> <img src="https://via.placeholder.com/200x80/ffffff/90c322.png?text=client+2" alt=""></div>
                    <div> <img src="https://via.placeholder.com/200x80/ffffff/90c322.png?text=client+3" alt=""></div>
                    <div> <img src="https://via.placeholder.com/200x80/ffffff/90c322.png?text=client+4" alt=""></div>
                    <div> <img src="https://via.placeholder.com/200x80/ffffff/90c322.png?text=client+5" alt=""></div>
                    <div> <img src="https://via.placeholder.com/200x80/ffffff/90c322.png?text=client+6" alt=""></div>
                    <div> <img src="https://via.placeholder.com/200x80/ffffff/90c322.png?text=client+7" alt=""></div>
                </div>	<!-- End of /.Owl-Slider -->
            </div>	<!-- End of /.col-md-12 -->
        </div> <!-- End Of /.Row -->
    </div> <!-- End Of /.Container -->
</section>	<!-- End of Section -->

<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nivoslider/3.2/jquery.nivo.slider.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // Owl carousel code
        $("#slider").nivoSlider();
        $("#owl-example").owlCarousel({
            autoPlay : true
        });
    });
</script>
<?= $this->endSection() ?>