<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<section id="topic-header">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h1>Products Details</h1>
                <p></p>
            </div>	<!-- /.col-md-4 -->
            <!--div class="col-md-8 hidden-xs">
                <ol class="breadcrumb pull-right">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Products</a></li>
                    <li class="active">Single Products</li>
                </ol>
            </div--> <!-- /.col-md-8 -->
        </div>	<!-- /.row -->
    </div>	<!-- /.container-->
</section><!-- /Section -->

<!-- PRODUCT Start
================================================== -->

<section id="single-product">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="single-product-img">
                    <?= anchor($product->image_large, img($product->image_medium, false, ['alt' => '']), ['class' => 'img-popup-link']); ?>
                </div>
            </div> <!-- End of /.col-md-5 -->
            <div class="col-md-4">
                <div class="block">
                    <div class="product-des">
                        <h4><?= $product->no; ?></h4>
                        <p class="crossref"><?= $product->cross_reference_no; ?></p>
                        <p class="detail"><?= $product->detail; ?></p>
                        <a class="view-link" href="#" style="width:70%; display:inline-block!important;">
                            <i class="fa fa-phone-square" aria-hidden="true"></i>Contact Us
                        </a>
                        <a class="view-link" href="<?= site_url("products/pdf/" . $product->id) ?>" style="width:28%; display:inline-block!important; background-color:orangered; cursor:pointer!important;">
                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF
                        </a>
                    </div>	<!-- End of /.product-des -->
                </div> <!-- End of /.block -->
            </div>	<!-- End of /.col-md-4 -->
            <div class="col-md-3">
                <div class="blog-sidebar">
                    <div class="block">
                        <h4 class="top-catagori-heading">Related Items</h4>
                        <ul class="media-list">
                            <?php
                                if ($product->related):
                                    foreach ($product->related as $row): ?>
                                    <li class="media">
                                        <?= anchor($row->url, img($row->image_square_thumbnail, false, ["class" => "media-object", "alt" => ""]), ["class" => "pull-left"]); ?>
                                        <div class="media-body">
                                            <?= anchor($row->url, $row->no."<p>Lorem ipsum dolor sit amet.</p>", ["class" => "media-heading"]); ?>
                                        </div>
                                    </li>	<!-- End of /.media -->
                                    <?php endforeach;
                                endif;
                            ?>
                        </ul>	<!-- End of /.media-list -->
                    </div>	<!-- End of /.block -->
                </div>	<!-- End of /.blog-sidebar -->
            </div>	<!-- End of /.col-md-3 -->
        </div>	<!-- End of /.row -->
        <div class="row">
            <div class="col-md-9">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab">More Info</a></li>
                    <li><a href="#specification" data-toggle="tab">Specifications</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <?= $product->description_2; ?>
                    </div>
                    <div class="tab-pane" id="specification">
                        <p>Sales Unit of Measure: <?= $product->uom; ?></p>
                        <p>Weight: <?= $product->weight; ?> kg</p>
                    </div>
                </div>
            </div>	<!-- End of /.col-md-9 -->
            <div class="col-md-3">
                <div class="blog-sidebar">
                    <div class="block">
                        <img src="https://via.placeholder.com/230x252/ffffff/90c322.png?text=Ads" alt="">
                    </div> <!-- End of /.block -->
                </div>	<!-- End of /.blog-sidebar -->
            </div>	<!-- End of /.col-md-3 -->
        </div>	<!-- End of /.row -->
    </div>	<!-- End of /.Container -->
</section> <!-- End of /.Single-product -->

<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".img-popup-link").magnificPopup({
            type: "image"
            // other options
        });

        $("#pdf_link").click(function() {
            var url = "<?= site_url(route_to('products/pdf')) ?>",
                id = "<?= $product->id ?>";

            $.post( url, { id: id })
            .done(function( data ) {

            });
        });
    });
</script>
<?= $this->endSection() ?>