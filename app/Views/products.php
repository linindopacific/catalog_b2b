<?= $this->extend($viewLayout) ?>

<?= $this->section('main') ?>
<section id="topic-header">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h1><?= $title ?? ''; ?></h1>
                <p><?= $subtitle ?? ''; ?></p>
            </div>	<!-- End of /.col-md-4 -->
            <!--div class="col-md-8 hidden-xs">
                <ol class="breadcrumb pull-right">
                    <li><a href="#">Home</a></li>
                    <li class="active">Products</li>
                </ol>
            </div-->	<!-- End of /.col-md-8 -->
        </div>	<!-- End of /.row -->
    </div>	<!-- End of /.container -->
</section>	<!-- End of /#Topic-header -->

<!-- PRODUCTS Start
================================================== -->

<section id="shop">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="products-heading">
                    <h2>LIST PRODUCTS</h2>
                </div>	<!-- End of /.Products-heading -->
                <div class="products-sorting">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Sorting:
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <?php
                                $uri1 = current_url(true);
                            ?>
                            <li class="<?= ($order == "asc") ? "active" : "" ?>">
                                <?php
                                    $uri1->addQuery("order", "asc");
                                ?>
                                <a href="<?= (string) $uri1 ?>">Sort by asc</a>
                            </li>
                            <li class="<?= ($order == "desc") ? "active" : "" ?>">
                                <?php
                                    $uri1->addQuery("order", "desc");
                                ?>
                                <a href="<?= (string) $uri1 ?>">Sort by desc</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="products_list" class="product-grid">
                    <ul>
                        <?php foreach($products as $product): ?>
                            <li>
                                <div class="products">
                                    <?= anchor($product->url, img($product->image_thumbnail, false, ["alt" => ""])); ?>
                                    <h4><?= $product->no; ?></h4>
                                    <p class="cross-reference"><?= $product->cross_reference_no; ?></p>
                                    <div>
                                        <a class="view-link shutter" href="<?= $product->url; ?>">
                                            <i class="fa fa-phone-square"></i>Contact Us
                                        </a>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>	<!-- End of /.products-grid -->
                <!-- Pagination -->
                <?= $pager->only(['search', 'order', 'category'])->links('default', 'frontend_full'); ?>
            </div>	<!-- End of /.col-md-9 -->
            <div class="col-md-3">
                <div class="blog-sidebar">
                    <div class="block">
                        <h4>Categories</h4>
                        <div class="catagorie">
                            <div class="list-group">
                                <?php foreach ($categories as $row): ?>
                                    <?php
                                        $category_name = $row->name;

                                        $uri2 = current_url(true);
                                        $uri2->setQuery("category=$category_name");
                                    ?>
                                    <a href="<?= (string) $uri2 ?>" class="list-group-item <?= ($category == $category_name) ? "catagorie-active" : "" ?>">
                                        <i class="fa fa-dot-circle-o"></i>
                                        <?= $category_name ?>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="block">
                        <img src="https://via.placeholder.com/230x252/ffffff/90c322.png?text=Ads" alt="">
                    </div>
                    <div class="block">
                        <h4>Popular Items</h4>
                        <ul class="media-list">
                            <?php for ($i=0; $i<3; $i++): ?>
                            <li class="media">
                                <a class="pull-left" href="<?= site_url('products/'. $i); ?>">
                                    <img class="media-object" src="https://via.placeholder.com/69x61/ffffff/90c322.png?text=Item" alt="...">
                                </a>
                                <div class="media-body">
                                    <a href="" class="media-heading">Lorem ipsum
                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </a>
                                </div>
                            </li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>
            </div>	<!-- End of /.col-md-3 -->
        </div>	<!-- End of /.row -->
    </div>	<!-- End of /.container -->
</section>	<!-- End of Section -->

<?= $this->endSection() ?>